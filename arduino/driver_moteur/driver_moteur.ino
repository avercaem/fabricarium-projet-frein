#include "tft.h"

#define LEFT 30
#define RIGHT 190

#define OKBTN 37
#define RIGHTBTN 25
#define LEFTBTN 26

#define ADC_PIN 39

#define MOTOREN 21
#define MOTORDIR 17
#define MOTORSTEP 22

#define SLOWEST_STEP 2000
#define FASTEST_STEP 100

TFT_eSPI tft = TFT_eSPI(135, 240); // Invoke custom library

int analog;
int pourcent;

int turn = false;
int dir = -1;

int savedAnalog, savedDir, savedTurn;


int okButtonState =0;
int leftButtonState =0;
int rightButtonState =0;

void espDelay(int ms)
{
  esp_sleep_enable_timer_wakeup(ms * 1000);
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_ON);
  esp_light_sleep_start();
}

uint16_t rgb(uint8_t red, uint8_t green, uint8_t blue){
  return (((red & 0b11111000)<<8) + ((green & 0b11111100)<<3)+(blue>>3));
}

void initColor(){  
  tft.fillScreen(rgb(255,255,255));

  tft.setTextColor(rgb(0,0,0));
  tft.setTextDatum(MC_DATUM);

  tft.setRotation(3);
  tft.setTextSize(2);
  tft.setCursor(0, 0);
}

void btnLoop(void *param){
  while (1){
    okButtonState = digitalRead(OKBTN);
    rightButtonState = digitalRead(RIGHTBTN);
    leftButtonState = digitalRead(LEFTBTN);
        
    if(okButtonState==HIGH){
      turn = !turn;
      savedAnalog = 0;
      vTaskDelay(500/portTICK_PERIOD_MS);
    }
    
    if(rightButtonState==HIGH){
      dir = 1;
      savedAnalog = 0;
      vTaskDelay(500/portTICK_PERIOD_MS);
    }
    
    if(leftButtonState==HIGH){
      dir = -1;
      savedAnalog = 0;
      vTaskDelay(500/portTICK_PERIOD_MS);
    }
    
    vTaskDelay(10/portTICK_PERIOD_MS);
  }
}

void motorLoop(void *param){
  while(1){
    if (turn==true){
      digitalWrite(MOTOREN, LOW);
    }
    else{
      digitalWrite(MOTOREN, HIGH);
    }

    if (dir==1){
      digitalWrite(MOTORDIR, LOW);
    }
    else{
      digitalWrite(MOTORDIR, HIGH);
    }
    
    int stepT =  map(pourcent, 0, 100, SLOWEST_STEP, FASTEST_STEP);
    Serial.println(stepT);
    digitalWrite(MOTORSTEP, HIGH);
    delayMicroseconds(1);
    digitalWrite(MOTORSTEP, LOW);
    delayMicroseconds(stepT);
  }
}

void setup(){
  Serial.begin(115200);
  Serial.println("Start");

  pinMode(OKBTN, INPUT);
  pinMode(LEFTBTN, INPUT);
  pinMode(RIGHTBTN, INPUT);

  pinMode(MOTOREN, OUTPUT);
  pinMode(MOTORDIR, OUTPUT);
  pinMode(MOTORSTEP, OUTPUT);
  
  
  tft.init();

  if (TFT_BL > 0) { 
    pinMode(TFT_BL, OUTPUT); 
    digitalWrite(TFT_BL, TFT_BACKLIGHT_ON);
  }
  tft.setSwapBytes(true);
 
  initColor();
  
  xTaskCreatePinnedToCore(btnLoop, "btnLoop", 1024, NULL, 1, NULL, 0);
  xTaskCreatePinnedToCore(motorLoop, "motorLoop", 1024, NULL, 1, NULL, 1);
}

void loop(){
  analog = map(analogRead(ADC_PIN), 0, 4096, LEFT, RIGHT);

  if(savedDir != dir or savedTurn != turn){
    savedDir = dir;
    savedTurn = turn;
    tft.fillScreen(rgb(255,255,255));
  }
  
  if(savedAnalog != analog){

    tft.setTextColor(rgb(255,255,255));

    tft.setTextSize(3);
    tft.drawString("^",  savedAnalog, 25);
    tft.setTextSize(2);

    tft.fillRect(80, 80, 240, 135,rgb(255,255,255));
    
    tft.setTextColor(rgb(0,0,0));
    
    savedAnalog = analog;

    pourcent = map(analog, LEFT, RIGHT, 0, 100);

    tft.setTextSize(2);
    tft.drawString("0%--------------100%",0,10);
    tft.setTextSize(3);
    tft.drawString("^",  analog, 25);
    tft.setTextSize(2);
    tft.drawString(String(pourcent)+"%",240,90);
    int steptime = map(analog, LEFT, RIGHT, SLOWEST_STEP, FASTEST_STEP);
    float trmin = 60.0/(steptime/1000.0*6.4);
    tft.drawString(String(trmin)+"tr/min",240,108);
    float mm = trmin*10;
    tft.drawString(String(mm)+"mm/min",240,135);

    tft.setTextSize(3);
    tft.setTextColor(rgb(255,0,255));
    if (dir == -1){
      tft.drawString("<-- : recule", 120, 55);
    }
    else{
      tft.drawString("--> : avance", 120, 55);
    }
    tft.setTextColor(rgb(0,0,0));
    tft.setTextSize(2);
    
    if(turn){
      tft.setTextColor(rgb(255,0,0));
      tft.drawString("Marche",0,135);
      tft.setTextColor(rgb(0,0,0));
    }
    else{
      tft.setTextColor(rgb(0,255,0));
      tft.drawString("Attente",0,135);
      tft.setTextColor(rgb(0,0,0));
    }
    vTaskDelay(50/portTICK_PERIOD_MS);
  }
}
