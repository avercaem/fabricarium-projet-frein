```bash

______                    _            _         _         __          _           
| ___ \                  | |          | |       | |       / _|        (_)          
| |_/ / __ _ _ __   ___  | |_ ___  ___| |_    __| | ___  | |_ _ __ ___ _ _ __  ___ 
| ___ \/ _` | '_ \ / __| | __/ _ \/ __| __|  / _` |/ _ \ |  _| '__/ _ \ | '_ \/ __|
| |_/ / (_| | | | | (__  | ||  __/\__ \ |_  | (_| |  __/ | | | | |  __/ | | | \__ \
\____/ \__,_|_| |_|\___|  \__\___||___/\__|  \__,_|\___| |_| |_|  \___|_|_| |_|___/
                                                                                   
                                                                                   

```
Boitier de contrôle : 
![V1 boitier controle](3D/boitier\ controle/preview_V1.jpg?raw=true "V1 boitier controle")

Protype de commande :
![V1 schéma compoisants](PCB/V1\ schéma\ composants.jpg?raw=true "V1 schéma compoisants")

Prévisualisation de l'attache moteur :
![V1 attache moteur](3D/attache\ moteur/preview_V1.jpg?raw=true "V1 attache moteur")

Par Arthur Vercaemst.

```bash
  A
  V
 ( )
```